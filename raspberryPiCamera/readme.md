﻿# OpenCV
OpenCV is an open source library, you'll need it to run the colorDetection python file.
## Installation
Verify that your raspberry Pi is on [Raspbian Stretch](https://www.raspberrypi.org/blog/raspbian-stretch/), if not you can download it [here](https://www.raspberrypi.org/downloads/raspbian/).
Then follow the [installation guide](https://www.pyimagesearch.com/2017/09/04/raspbian-stretch-install-opencv-3-python-on-your-raspberry-pi/) (make sure you have time, the installation takes between 2 hours and 4 hours)
