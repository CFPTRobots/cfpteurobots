#!/usr/bin/env python3
"""
CFPT Eurobots
Description:
    This program uses the raspberry pi camera and display
    the frames while removing all colors except red with
    sci-kit instead of openCV.
Author: Nelson Jeanrenaud <nelson.jnrnd@eduge.ch>
Version: 1.0.0
Date: 03.04.2019
"""

# import needed packages
from picamera.array import PiRGBArray
from picamera import PiCamera

from skimage import data, color, img_as_ubyte
from skimage.feature import canny
from skimage.transform import hough_ellipse
from skimage.draw import ellipse_perimeter
import time


# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 30
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)


# capture frames from the camera
for frame in camera.capture_continuous(
        rawCapture, format="bgr", use_video_port=True):

    # grab the raw NumPy array representing the image,
    # then initialize the timestamp
    actualFrame = frame.array

    # Load picture, convert to grayscale and detect edges
    img = color.rgb2gray(actualFrame)
    image = img_as_ubyte(img)
    edges = canny(image, sigma=3, low_threshold=10, high_threshold=50)

    hough_radii = np.arange(min_diameter, max_diameter, step_diameter)

    hough_res = hough_circle(edges, hough_radii)

    accums, cx, cy, radii = hough_circle_peaks(hough_res, hough_radii,
                                               total_num_peaks=number_best_circle, min_xdistance=20,
                                               min_ydistance=20)

    print(zip(accums, cx, cy, radii))
    ax1.set_title('Original picture')
    ax1.imshow(image)





    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)
