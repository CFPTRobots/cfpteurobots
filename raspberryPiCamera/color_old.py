#! /usr/bin python3
# coding=utf-8

# import needed packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy as np
import cv2
import imutils
import time
import tkinter

resX = 192
resY = 192
fps = 18
# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (resX, resY)
camera.framerate = fps
rawCapture = PiRGBArray(camera, size=camera.resolution)
# allow the camera to warmup
time.sleep(0.1)
# initialize the tkinter form
outputWindow = tkinter.Tk()
outputText = tkinter.StringVar()
tkinter.Label(outputWindow, textvariable=outputText).pack()
outputWindow.resizable(0, 0)
outputWindow.geometry("500x500")

# define the boundaries of the detected color (red)
lower_red = np.array([170, 70, 50])
upper_red = np.array([180, 255, 255])

horizontalCenter = camera.resolution.width / 2
position = [None, None]

# capture frames from the camera
for frame in camera.capture_continuous(
        rawCapture, format="bgr", use_video_port=True):

    # grab the raw NumPy array representing the image,
    image = frame.array

    # blurs the image to have smoother shapes
    blurred = cv2.GaussianBlur(image, (11, 11), 0)

    # convert the image to the HSV color space, create a mask
    # with only the color "red" and remove any small defaults in the image
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_red, upper_red)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # find the contours
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    center = None

    # check to ensure at least one object was found in our frame
    if len(cnts) > 0:
        # find the biggest contour and calculate it's radius coordinates and radius
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        # get the X and Y position of the center point
        position[0] = int(M["m10"] / M["m00"])
        position[1] = int(M["m01"] / M["m00"])
        center = (position[0], position[1])
        # draw the point and circle the object on the display
        cv2.circle(image, (int(x), int(y)), int(radius),
                   (0, 255, 255), 2)
        cv2.circle(image, center, 5, (0, 0, 255), -1)

        if position[0] > horizontalCenter:
            outputText.set("Droite")
        else:
            outputText.set("Gauche")
    else:
        outputText.set("Nothing detected")

    Tk.update(outputWindow)

    # show the frame
    # cv2.imshow("Color Detect", mask)
    cv2.imshow("No filter", image)
    # cv2.moveWindow("No filter", 800, 50)

    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # if the `q` key was pressed, break from the loop
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        outputWindow.destroy()
        break