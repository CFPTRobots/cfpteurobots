#!/usr/bin/env python3

# import needed packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy as np
import cv2
import imutils
import time
import tkinter
#from matplotlib import pyplot as plt

import rospy
from std_msgs.msg import Int32MultiArray

# Talker ROS BUS
def talker(message):
    pub = rospy.Publisher('chatter', Int32MultiArray, queue_size=10)
    rospy.init_node('talker', anonymous=True)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        my_array = Int32MultiArray(data=message)
        pub.publish(my_array)
        rate.sleep()

resX = 192
resY = 192
fps = 18
circleColor = (0, 0, 255) # red
circleRadius = 5
lineThickness = 2
fillCircle = -1
# Gaussian Kernel Size. [height, width]. height and width should be odd and can have different values.
blurIntensity = (11, 11)

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (resX, resY)
camera.framerate = fps
rawCapture = PiRGBArray(camera, size=camera.resolution)
# allow the camera to warmup
time.sleep(0.1)
# initialize the tkinter form
outputWindow = tkinter.Tk()
outputText = tkinter.StringVar()
tkinter.Label(outputWindow, textvariable=outputText).pack()
outputWindow.resizable(0, 0)

outputWindow.geometry("500x500")

# define the boundaries of the detected color (red)
lower_red = np.array([170, 70, 50])
upper_red = np.array([180, 255, 255])

horizontalCenter = camera.resolution.width / 2
position = [None, None]

# capture frames from the camera
for frame in camera.capture_continuous(
        rawCapture, format="bgr", use_video_port=   True):

    # grab the raw NumPy array representing the image,
    image = frame.array

    # blurs the image to have smoother shapes
    blurred = cv2.GaussianBlur(image, blurIntensity, 0)

    # convert the image to the HSV color space, create a mask
    # with only the color "red" and remove any small defaults in the image
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_red, upper_red)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # find the contours
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    center = None
    # check to ensure at least one object was found in our frame
    if len(cnts) > 0:
        # find the biggest contour and calculate it's radius coordinates and radius
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        # get the X and Y position of the center point
        position[0] = int(M["m10"] / M["m00"])
        position[1] = int(M["m01"] / M["m00"])
        center = (position[0] , position[1])
        # draw the point and circle the object on the display
        cv2.circle(image, (int(x), int(y)), int(radius),
				   circleColor, lineThickness)
        cv2.circle(image, center, circleRadius, circleColor, fillCircle)

        if position[0] > horizontalCenter:
            outputText.set("Droite")
        else:
            outputText.set("Gauche")
    else:
        outputText.set("Nothing detected")

    tkinter.Tk.update(outputWindow)

    # show the frame
    cv2.imshow("No filter", image)
    #plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
    #plt.xticks([]), plt.yticks([])
    #plt.show()
    my_array = Int32MultiArray(data=center)
    pub.publish(my_array)

    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    # if the `q` key was pressed, break from the loop
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        outputWindow.destroy()
        break
