#!/usr/bin/env python3
"""
basic demo of the lidar with 4 square that get lighter when something is close of that angle
"""

from tkinter import Tk
from lidardemoview import LidarDemoView

def main():
    """
    basic demo of the lidar
    """
    root = Tk()
    demo = LidarDemoView()
    root.geometry("250x70")
    while True:
        demo.update_view()
        root.update_idletasks()
        root.update()

if __name__ == '__main__':
    main()
