#!/usr/bin/env python3
"""
Programm that reads data from the LDS and sends it to a listener
"""

#import datetime
import os
from platform import uname
from subprocess import Popen, PIPE
from std_msgs.msg import Float32MultiArray
import rospy

#Sets the good name for the driver depending on the type of the processor
LDS_BIN_FILENAME = "lds_driver."
SYSTEM_PROCESSOR_TYPE = uname()[4]
if SYSTEM_PROCESSOR_TYPE == "x86_64":
    LDS_BIN_FILENAME += "x86_64"
elif SYSTEM_PROCESSOR_TYPE == "armv7l":
    LDS_BIN_FILENAME += "arm32"
else:
    raise FileNotFoundError("No file for this type of processor")

#Gets the full path for the driver of the LDS
LDS_BIN = os.path.realpath(__file__)
LDS_BIN = LDS_BIN[:LDS_BIN.rfind(os.sep)] + os.sep + LDS_BIN_FILENAME

def main():
    """
    Code that sends the value of the LDS to a listener
    """
    #Starts the driver in the background as a subprocess
    process = Popen([LDS_BIN],
                    stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False)

    #Initialising the bus talker object
    publisher = rospy.Publisher('chatter', Float32MultiArray, queue_size=10)
    rospy.init_node('talker', anonymous=True)

    #d_current = datetime.datetime.now()
    while not rospy.is_shutdown():
        #d_current = datetime.datetime.now() - d_current
        #print("elapsed=" + d_current.second)

        #Reads a char from the driver
        line = process.stdout.readline()
        line = line.decode("ascii")

        if line != "":
            #Reads the angle and the value and put it into an array
            splitted_stream = line.split("=")
            value = float(splitted_stream[1])
            dirty_angle = splitted_stream[0]
            str_angle = ""
            for char in dirty_angle:
                if str(char).isdecimal():
                    str_angle += char

            angle = int(str_angle)
            lds_values = [angle, value]

            #Creates a special array that can be used by the publisher
            my_array = Float32MultiArray(data=lds_values)
            publisher.publish(my_array)
            print("Angle: {0:0>3}, Value: {1:0<5}".format(angle, value))
    #Kills the process because it doesn't do it by default when you do ctrl+C
    process.terminate()

if __name__ == '__main__':
    main()
