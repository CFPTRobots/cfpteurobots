#!/usr/bin/env python3
"""
Programm that reads data from the LDS and sends it to a listener
"""

from os import path
from subprocess import Popen, PIPE, run
from threading import Thread
from pathlib import Path
import logging
import sys

# Gets the full path for the driver of the LDS
LDS_DRIVER = Path(
    "hls_lfcd_lds_driver/applications/lds_driver/lds_driver").resolve()


class Lidar(Thread):
    """
    Class made for the LDS on the TurtleBot Waffle Pi
    """

    def __init__(self):
        # Sets logging to stderr and at DEBUG level
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
        logging.debug(LDS_DRIVER)
        # Replaces the init function of Thread by this one
        Thread.__init__(self)
        # Says that this program can be killed when the other programs are killed
        self.daemon = True
        # Check if the driver is here, if not, compiles it
        if not LDS_DRIVER.exists():
            logging.info("driver not here")
            self.make_driver()
        # Starts the driver in the background as a subprocess
        logging.debug("opening the driver")
        self.process = Popen([str(LDS_DRIVER)],
                             stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False)
        # Initialise the array with 360 None so we can fill them later
        self.lds_values = [None] * 360
        # Starts the function run in an other thread
        self.start()

    def run(self):
        """
        Reads the output of the lds_driver and saves it
        """
        while True:
            # Reads a char from the driver
            line = self.process.stdout.readline()
            line = line.decode("ascii")

            if line:
                # Reads the angle and the value and put it into an array
                splitted_stream = line.split("=")
                value = float(splitted_stream[1])
                angle = int(splitted_stream[0])

                self.lds_values[angle] = value
        # Kills the process because it doesn't do it by default when you do ctrl+C
        self.process.terminate()

    @staticmethod
    def make_driver():
        """
        Builds the driver
        """
        logging.info("creating driver...")
        # Runs the command "make" where the driver should be to build it
        output = run(
            "make",
            shell=True,
            check=True,
            stdout=PIPE,
            stderr=PIPE,
            cwd=str(path.dirname(LDS_DRIVER))
        )
        logging.debug(output.stdout.decode("ascii"))
        logging.info("driver created")

    def get_distance(self, angle=None):
        """
        Gets the distance from the lidar via an angle
        """
        if angle is not None:
            distance = self.lds_values[angle]
            if distance is None:
                distance = 0

            # logging.debug(distance)
            return distance

        return self.lds_values

    def get_avg_distance(self, min_angle, max_angle):
        """
        Gets the average distance between two limit angles
        """
        avg_distance = 0
        if min_angle > max_angle:
            logging.error(
                "in get_avg_distance: min_angle should not be bigger than max_angle")
        else:
            for angle in range(min_angle, max_angle + 1):
                avg_distance += self.get_distance(angle)
            avg_distance = avg_distance / (max_angle + 1 - min_angle)

        # logging.debug(avg_distance)
        return avg_distance
