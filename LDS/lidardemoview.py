#!/usr/bin/env python3
"""
basic demo of the lidar
"""

from tkinter import Frame, BOTH, Canvas
from lidar import Lidar

DISTANCE_TRESHOLD = 0.1


class LidarDemoView(Frame):
    """
    class for a basic demo of the lidar
    """

    def __init__(self):
        super().__init__()
        self.lidar = Lidar()
        self.initui()

    def initui(self):
        """
        Initialise the window
        """
        self.master.title("Lidar")
        self.pack(fill=BOTH, expand=1)

        self.canvas = Canvas(self)
        self.canvas.create_rectangle(10, 10, 50, 50, fill="#fb0")
        self.canvas.pack(fill=BOTH, expand=1)

    def update_view(self):
        """
        updates the windows
        """
        avg_square_one = self.lidar.get_avg_distance(0, 89)
        avg_square_two = self.lidar.get_avg_distance(90, 179)
        avg_square_three = self.lidar.get_avg_distance(180, 269)
        avg_square_four = self.lidar.get_avg_distance(270, 359)

        if avg_square_one <= DISTANCE_TRESHOLD and avg_square_one > 0:
            self.canvas.create_rectangle(10, 10, 60, 60, fill="green")
        else:
            self.canvas.create_rectangle(10, 10, 60, 60, fill="dark green")

        if avg_square_two <= DISTANCE_TRESHOLD and avg_square_two > 0:
            self.canvas.create_rectangle(70, 10, 120, 60, fill="dodger blue")
        else:
            self.canvas.create_rectangle(70, 10, 120, 60, fill="blue")

        if avg_square_three <= DISTANCE_TRESHOLD and avg_square_three > 0:
            self.canvas.create_rectangle(130, 10, 180, 60, fill="red")
        else:
            self.canvas.create_rectangle(130, 10, 180, 60, fill="red4")

        if avg_square_four <= DISTANCE_TRESHOLD and avg_square_four > 0:
            self.canvas.create_rectangle(190, 10, 240, 60, fill="salmon1")
        else:
            self.canvas.create_rectangle(190, 10, 240, 60, fill="salmon4")

        self.canvas.pack(fill=BOTH, expand=1)
