#!/usr/bin/env python3

from motor import Motor
import rospy
from std_msgs.msg import Int32

m = Motor()

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)

    if data.data == 2:
        m.forward()
    elif data.data == 3:
        m.rotate_left()
    elif data.data == 1:
        m.rotate_right()
    else:
        m.stop()

def listener():

    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('chatter', Int32, callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
