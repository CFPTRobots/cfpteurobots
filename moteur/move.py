#!/usr/bin/env python3
import RPi.GPIO as GPIO
import time
import keyboard

try:
    # Initialisation des constantes
    # Moteur Gauche
    ENA = 13
    IN1 = 6
    IN2 = 5
    # Moteur Droit
    ENB = 18
    IN3 = 14
    IN4 = 15

    GPIO.setmode(GPIO.BCM)

    #GPIO.setwarnings(False) # to avoid warnings
    # Initialisation des pins
    GPIO.setup(IN1,GPIO.OUT)
    GPIO.setup(IN2,GPIO.OUT)
    GPIO.setup(ENA,GPIO.OUT)

    GPIO.setup(IN3,GPIO.OUT)
    GPIO.setup(IN4,GPIO.OUT)
    GPIO.setup(ENB,GPIO.OUT)

    def Front(): # turns forward
        GPIO.output(IN1,GPIO.HIGH)
        GPIO.output(IN2,GPIO.LOW)
        GPIO.output(ENA,GPIO.HIGH)

        GPIO.output(IN3,GPIO.HIGH)
        GPIO.output(IN4,GPIO.LOW)
        GPIO.output(ENB,GPIO.HIGH)
    def Back(): # turns backward
        GPIO.output(IN1,GPIO.LOW)
        GPIO.output(IN2,GPIO.HIGH)
        GPIO.output(ENA,GPIO.HIGH)

        GPIO.output(IN3,GPIO.LOW)
        GPIO.output(IN4,GPIO.HIGH)
        GPIO.output(ENB,GPIO.HIGH)
    def LeftTurn(): # turns left
        GPIO.output(IN1,GPIO.LOW)
        GPIO.output(IN2,GPIO.HIGH)
        GPIO.output(ENA,GPIO.HIGH)

        GPIO.output(IN3,GPIO.HIGH)
        GPIO.output(IN4,GPIO.LOW)
        GPIO.output(ENB,GPIO.HIGH)
    def RightTurn(): # turns right
        GPIO.output(IN1,GPIO.HIGH)
        GPIO.output(IN2,GPIO.LOW)
        GPIO.output(ENA,GPIO.HIGH)

        GPIO.output(IN3,GPIO.LOW)
        GPIO.output(IN4,GPIO.HIGH)
        GPIO.output(ENB,GPIO.HIGH)
    def Stop(): #set all pins to low
        GPIO.output(IN1,GPIO.LOW)
        GPIO.output(IN2,GPIO.LOW)
        GPIO.output(ENA,GPIO.LOW)

        GPIO.output(IN3,GPIO.LOW)
        GPIO.output(IN4,GPIO.LOW)
        GPIO.output(ENB,GPIO.LOW)
    #p1 = GPIO.PWM(18, 50)  # channel=18 frequency=50Hz   #           18 to change direction
    #p1.start(0)
    #p2 = GPIO.PWM(13, 50)  # channel=13 frequency=50Hz   #           18 to change direction
    #p2.start(0)
    while True:
        if keyboard.is_pressed('w'):
            Front()
        if keyboard.is_pressed('s'):
            Back()
        if keyboard.is_pressed('a'):
            LeftTurn()
        if keyboard.is_pressed('d'):
            RightTurn()
        if keyboard.is_pressed('esc'):
            Stop()
            GPIO.cleanup()
            break
        else:
            Stop()
except:
    Stop()
    GPIO.cleanup()
