#!/usr/bin/env python3
"""
Robot that move around and turns when he sees a wall
"""

import logging
import time
from lidar import Lidar
from motor import Motor

ANGLE_FRONT = 0
DISTANCE_TRESHOLD = 0.70


def main():
    """
    Main Function
    """
    lidar = Lidar()
    motor = Motor()

    while True:
        distance = lidar.get_distance(ANGLE_FRONT)
        # print(distance)
        if 0 < distance < DISTANCE_TRESHOLD:
            motor.rotate_left()
            #logging.debug("GO LEFT")
            time.sleep(0.5)
        else:
            motor.forward()
            #logging.debug("GO DEVANT")


if __name__ == '__main__':
    main()
