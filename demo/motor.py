#!/usr/bin/env python3
import RPi.GPIO as GPIO

PWM_SPEED = 70

class Motor():
    """
    Class to controls the motors of our robot
    """
    
    def __init__(self, ENA=13, IN1=6, IN2=5, ENB=18, IN3=14, IN4=15):
        # Pins for the left motor
        self.ENA = ENA
        self.IN1 = IN1
        self.IN2 = IN2
        # Pings for the right motor
        self.ENB = ENB
        self.IN3 = IN3
        self.IN4 = IN4

        GPIO.setmode(GPIO.BCM)

        # GPIO.setwarnings(False) # to avoid warnings
        # Initialize the GPIO pin as output ports
        GPIO.setup(self.IN1, GPIO.OUT)
        GPIO.setup(self.IN2, GPIO.OUT)
        GPIO.setup(self.ENA, GPIO.OUT)

        GPIO.setup(self.IN3, GPIO.OUT)
        GPIO.setup(self.IN4, GPIO.OUT)
        GPIO.setup(self.ENB, GPIO.OUT)

        # Created pwm object with a period of 100 milliseconds and a duty cylcle of 0 %
        self.pwmLeft = GPIO.PWM(self.ENA, 100)
        self.pwmLeft.start(0)
        self.pwmRight = GPIO.PWM(self.ENB, 100)
        self.pwmRight.start(0)

    # !!!!!!! Remove ENB and ENA High for acceleration !!!!!!
    def change_wheel_rotation(self, direction='forward', side='left'):
        directions = ['forward', 'backward']
        sides = ['left', 'right']
        # Check if input syntax is correct
        if direction not in directions:
            raise ValueError("Invalid direction. Expected one of: %s" % directions)
        if side not in sides:
            raise ValueError("Invalid side. Expected one of: %s" % sides)

        if direction == 'forward' and side == 'left':
            GPIO.output(self.IN1, GPIO.HIGH)
            GPIO.output(self.IN2, GPIO.LOW)
#            GPIO.output(self.ENA, GPIO.HIGH)
        elif direction == 'forward' and side == 'right':
            GPIO.output(self.IN3, GPIO.HIGH)
            GPIO.output(self.IN4, GPIO.LOW)
#            GPIO.output(self.ENB, GPIO.HIGH)
        elif direction == 'backward' and side == 'left':
            GPIO.output(self.IN1, GPIO.LOW)
            GPIO.output(self.IN2, GPIO.HIGH)
#            GPIO.output(self.ENA, GPIO.HIGH)
        elif direction == 'backward' and  side == 'right':
            GPIO.output(self.IN3, GPIO.LOW)
            GPIO.output(self.IN4, GPIO.HIGH)
#            GPIO.output(self.ENB, GPIO.HIGH)

    def forward(self):
        self.change_wheel_rotation('forward', 'left')
        self.change_wheel_rotation('forward', 'right')
        self.pwmLeft.ChangeDutyCycle(PWM_SPEED)
        self.pwmRight.ChangeDutyCycle(PWM_SPEED)

    def backward(self):
        self.change_wheel_rotation('backward', 'left')
        self.change_wheel_rotation('backward', 'right')

    def stop(self):
        GPIO.output(self.IN1, GPIO.LOW)
        GPIO.output(self.IN2, GPIO.LOW)
        GPIO.output(self.ENA, GPIO.LOW)

        GPIO.output(self.IN3, GPIO.LOW)
        GPIO.output(self.IN4, GPIO.LOW)
        GPIO.output(self.ENB, GPIO.LOW)

    def rotate_left(self):
        self.change_wheel_rotation('backward', 'left')
        self.change_wheel_rotation('forward', 'right')
        self.pwmLeft.ChangeDutyCycle(PWM_SPEED)
        self.pwmRight.ChangeDutyCycle(PWM_SPEED)

    def rotate_right(self):
        self.change_wheel_rotation('forward', 'left')
        self.change_wheel_rotation('backward', 'right')

    def acceleration(self, side='left', dutyCycleIncrease=10, isDecelerating=False):
        sides = ['left', 'right']
        # Check if input syntax is correct
        if side not in sides:
            raise ValueError("Invalid side. Expected one of: %s" % sides)
        if isDecelerating:
            dutyCycleIncrease = -1 * dutyCycleIncrease

        if side == 'left':
            self.pwmLeft.ChangeDutyCycle(dutyCycleIncrease)
        elif side == 'right':
            self.pwmRight.ChangeDutyCycle(dutyCycleIncrease)
