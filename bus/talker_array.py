#!/usr/bin/env python3

import rospy
from std_msgs.msg import Int32MultiArray

def talker():
    pub = rospy.Publisher('chatter', Int32MultiArray, queue_size=10)
    rospy.init_node('talker', anonymous=True)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        message = [55145, 5615632, 76516, 846695]
        #my code here
        my_array = Int32MultiArray(data=message)
        pub.publish(my_array)
	#rate.sleep()

if __name__ == '__main__':
    talker()
