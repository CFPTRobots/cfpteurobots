#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32MultiArray


def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)


def listener():

    rospy.init_node('listener', anonymous=True)
    #Changer le String par le format de donnée voulue en fonction du format à 
    #recevoir 
    rospy.Subscriber('chatter', Float32MultiArray, callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
