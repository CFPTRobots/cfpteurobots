# CFPTEurobots
## Mise en place de l'environement de développement

### Requirements
Pour pouvoir lancer les programmes dans ce git il faut installer les requirements (c'est ce que vous chargez quand vous tapez `import truc`). Pour ce faire vous devez vous mettre à la racine du git et executer: `python3 -m pip install -r requirements.txt`. Si vous faites ceci sur un raspberry pi, il faut commenter la ligne sur `opencv-python` et installer opencv depuis les fichiers sources. Un tutoriel ce trouve dans le google drive du projet.

### Submodules
Ce projet utilise des submodules, c'est un peu comme des gits dans des gits. Pour ce faire il faut utiliser une commande comme `git submodule update --init`. Le submodule que nous utilisons dans notre projet est le "hls_lfcd_lds_driver" qui contient, comme son nom l'indique, le driver pour le LDS. 

### Patch
Nous avons fait des changements dans le code du driver, pour les appliquer, placez vous a la racine du git et executez cette commande `patch hls_lfcd_lds_driver/applications/lds_driver/lds_driver.cpp patch/lds_driver.patch`.

### Driver
Nous avons modifié le code source du driver, il faut maintenant le build. Placez vous dans le dossier hls_lfcd_lds_driver/applications/lds_driver/ et exectuez la commande `make`. Il n'est pas nécéssaire de déplacer le fichier. Normalement le programme `lidar.py` se charge de build le driver si cela n'as pas été fait avant.
Attention de ne pas utiliser le même driver entre un ordinateur et un raspberry pi. Ils n'utilisent pas le type de processeur (x86_64 et arm).

### Recapitulons
Alors voici toutes les commandes que vous devriez avoir executé (vous pouvez copier coller ça dans votre terminal)
``` bash
python3 -m pip install -r requirements.txt
git submodule update --init
patch hls_lfcd_lds_driver/applications/lds_driver/lds_driver.cpp patch/lds_driver.patch
cd hls_lfcd_lds_driver/applications/lds_driver
make
```